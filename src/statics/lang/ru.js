export default {
  isoName: 'ру',
  nativeName: 'английский(UK)',
  label: {
    clear: 'очистить',
    ok: 'OK',
    cancel: 'отмена',
    close: 'закрыть ',
    set: 'установка',
    select: 'выбрать',
    reset: 'сброс',
    remove: 'удалить',
    update: 'обновление',
    create: 'создать',
    search: 'поиск',
    filter: 'фильтр',
    refresh: 'обновление'
  },
  date: {
    days: 'понедельник, вторник, среда, четверг,пятница, суббота'.split('_'),
    daysShort: 'пн_вт_ср_чт_пт_сб'.split('_'),
    months:'январь, февраль, март, апрель, май, июнь, июль, август,сентябрь, октябрь, ноябрь, декабрь'.split('_'),
    monthsShort: 'янв, фев, март, апр, май, июнь, июль, авг, сент, окт, ноя, дек'.split('_'),
    firstDayOfWeek: 1, // 0-6, 0 - Sunday, 1 Monday, ...
    format24h: true
  },
  table: {
    noData: 'данные недоступные ',
    noResults: 'не найдено подходящей записи',
    loading: 'загружается...',
    selectedRecords: function (rows) {
      return rows === 1
        ? '1 запись выбрана.'
        : (rows === 0 ? 'No' : rows) + 'записи выбраны.'
    },
    recordsPerPage: 'записи по странице:',
    allRows: 'все',
    pagination: function (start, end, total) {
      return start + '-' + end + ' of ' + total
    },
    columns: столбец'
  },
  editor: {
    url: 'урл',
    bold: 'жирнный',
    italic: 'курсив',
    strikethrough: 'зачёркивание',
    underline: 'подчеркнуть',
    unorderedList: 'неупорядоченный список',
    orderedList: 'упорядоченный списо',
    subscript: 'Subscript',
    superscript: 'список индексов',
    hyperlink: 'гиперлинк',
    toggleFullscreen: 'режим польного экрана',
    quote: ссылка',
    left: 'выровнять по левому краю',
    center: 'Установка знаков в центре',
    right: 'выровнять по левому краю',
    justify: 'выровнять по ширине',
    print: 'печать',
    outdent: 'уменьшить отступ',
    indent: 'увеличить отступ',
    removeFormat: 'удалить форматирование',
    formatting: ' форматирование',
    fontSize: 'размер шрифта',
    align: 'выровнять',
    hr: 'Горизонтальная линейка',
    undo: 'аннулировать',
    redo: 'переделывать',
    heading1: 'заголовок 1',
    heading2: 'заголовок 2',
    heading3: 'заголовок 3',
    heading4: 'заголовок 4',
    heading5: 'заголовок 5',
    heading6: 'заголовок 6',
    paragraph: абзац',
    code: 'код',
    size1: 'очень малый ',
    size2: немного малый ',
    size3: 'стандартный',
    size4: 'средний большой',
    size5: 'большой',
    size6: 'очень большой ',
    size7: 'максимальный ',
    defaultFont: 'шрифт по умолчанию',
    viewSource: 'просмотр источника'
  },
  tree: {
    noNodes: 'нет доступных узлов',
    noResults: подходящих узлов не найдено'
  }
}