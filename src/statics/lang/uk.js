// These default language files are downloaded from https://github.com/quasarframework/quasar/tree/dev/ui/lang
// Feel free to update them from source.

export default {
  isoName: 'en-gb',
  nativeName: 'Англійська (UK)',
  label: {
    clear: 'зрозуміло',
    ok: 'OK',
    cancel: 'Скасувати',
    close: 'Закрити',
    set: 'Встановити',
    select: 'Вибрати',
    reset: 'Скинути',
    remove: 'Видалити',
    update: 'Оновити',
    create: 'Створити',
    search: 'Пошук',
    filter: 'Фільтрувати',
    refresh: 'Оновити'
  },
  date: {
    days: 'Неділя_Понеділок_Вівторок_Середа_Четвер_П\'ятниця_Субота'.split('_'),
    daysShort: 'Нд_Пн_Вт_Ср_Чт_Пт_Сб'.split('_'),
    months: 'Січень_Лютий_Березень_Квітень_Травень_Червень_Липень_Серпень_Вересень_Жовтень_Листопад_Грудень'.split('_'),
    monthsShort: 'Січ_Лют_Берез_Квіт_Трав_Черв_Лип_Серп_Верес_Жовт_Листоп_Груд'.split('_'),
    firstDayOfWeek: 1, // 0-6, 0 - Sunday, 1 Monday, ...
    format24h: true
  },
  table: {
    noData: 'Немає даних',
    noResults: 'Не знайдено відповідних записів',
    loading: 'Завантаження...',
    selectedRecords: function (rows) {
      return rows === 1
        ? 'Вибрано 1 запис.'
        : (rows === 0 ? 'Ні' : rows) + ' вибрані записи.'
    },
    recordsPerPage: 'Записів на сторінку:',
    allRows: 'Всі',
    pagination: function (start, end, total) {
      return start + '-' + end + ' of ' + total
    },
    columns: 'Колонки'
  },
  editor: {
    url: 'адреса URL',
    bold: 'Жирний',
    italic: 'Курсив',
    strikethrough: 'Закреслення',
    underline: 'Підкреслення',
    unorderedList: 'Неупорядкований список',
    orderedList: 'Упорядкований список',
    subscript: 'Нижній індекс',
    superscript: 'Верхній індекс',
    hyperlink: 'Гіперпосилання',
    toggleFullscreen: 'Увімкнути повноекранний режим',
    quote: 'Цитата',
    left: 'Вирівняти по лівому краю',
    center: 'Вирівняти по центру',
    right: 'Вирівняти по правому краю',
    justify: 'Вирівняти по ширині',
    print: 'Друк',
    outdent: 'Зменшити відступ',
    indent: 'Збільшити відступ',
    removeFormat: 'Видалити форматування',
    formatting: 'Форматування',
    fontSize: 'Розмір шрифту',
    align: 'Вирівняти',
    hr: 'Вставити горизонтальну лінійку',
    undo: 'Скасувати',
    redo: 'Повторити',
    heading1: 'Заголовок 1',
    heading2: 'Заголовок 2',
    heading3: 'Заголовок 3',
    heading4: 'Заголовок 4',
    heading5: 'Заголовок 5',
    heading6: 'Заголовок 6',
    paragraph: 'Абзац',
    code: 'Код',
    size1: 'Найменший',
    size2: 'Трохи маленький',
    size3: 'Звичайний',
    size4: 'Середній',
    size5: 'Великий',
    size6: 'Дуже великий',
    size7: 'Найбільший',
    defaultFont: 'Шрифт за замовчуванням',
    viewSource: 'Переглянути джерело'
  },
  tree: {
    noNodes: 'Немає доступних вузлів',
    noResults: 'Не знайдено відповідних вузлів'
  }
}
