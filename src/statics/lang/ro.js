export default {
  isoName: 'en-gb',
  nativeName: 'Engleză (UK)',
  label: {
    clear: 'Liber',
    ok: 'OK',
    cancel: 'Anulează',
    close: 'Închide',
    set: 'Configurează',
    select: 'Selectează',
    reset: 'Resetează',
    remove: 'Elimină',
    update: 'Actualizează',
    create: 'Creează',
    search: 'Caută',
    filter: 'Filtrează',
    refresh: 'Reîncarcă'
  },
  date: {
    zile: 'duminică_luni_marți_miercuri_joi_vineri_sâmbătă'.split('_'),
    daysShort: 'dum_lun_mar_mie_joi_vin_sâm'.split('_'),
    months: 'ianuarie_februarie_martie_aprilie_mai_iunie_iulie_august_septembrie_octombrie_noiembrie_decembrie'.split('_'),
    monthsShort: 'ian_feb_mar_apr_mai_iun_iul_aug_sep_oct_nov_dec'.split('_'),
    firstDayOfWeek: 1, // 0-6, 0 - Sunday, 1 Monday, ...
    format24h: true
  },
  table: {
    noData: 'Nu sunt date disponibile',
    noResults: 'Nu au fost găsite fișiere corespunzătoare',
    loading: 'Se încarcă...',
    selectedRecords: function (rows) {
      return rows === 1
        ? '1 fișier selectat.'
        : (rows === 0 ? 'Nu' : rows) + ' fișiere selectate.'
    },
    recordsPerPage: 'Fișiere de pagină:',
    allRows: 'All',
    pagination: function (start, end, total) {
      return start + '-' + end + ' of ' + total
    },
    columns: 'Coloane'
  },
  editor: {
    url: 'URL',
    bold: 'aldin',
    italic: 'cursiv',
    strikethrough: 'tăiat',
    underline: 'subliniat',
    unorderedList: 'Listă nesortată',
    orderedList: 'Listă sortată',
    subscript: 'Subscript',
    superscript: 'Superscript',
    hyperlink: 'Hyperlink',
    toggleFullscreen: 'Toggle Fullscreen',
    quote: 'Citat',
    left: 'Aliniere la stânga',
    center: 'Aliniere centrată',
    right: 'Aliniere la dreapta',
    justify: 'Aliniere stânga-dreapta',
    print: 'Imprimare',
    outdent: 'Reducerea indentării',
    indent: 'Creșterea indentării',
    removeFormat: 'Eliminarea formatării',
    formatting: 'Formatare',
    fontSize: 'Mărime font',
    align: 'Aliniere',
    hr: 'Introducere riglă orizontală',
    undo: 'Anulare ',
    redo: 'Repetă',
    heading1: 'Titlu 1',
    heading2: 'Titlu 2',
    heading3: 'Titlu 3',
    heading4: 'Titlu 4',
    heading5: 'Titlu 5',
    heading6: 'Titlu 6',
    paragraph: 'Paragraf',
    code: 'Cod',
    size1: 'Foarte mic',
    size2: 'Destul de mic',
    size3: 'Normal',
    size4: 'Mediu-mare',
    size5: 'Mare',
    size6: 'Foarte mare',
    size7: 'Maximum',
    defaultFont: 'Font implicit',
    viewSource: 'Vezi sursa'
  },
  tree: {
    noNodes: 'Nu sunt disponibile noduri',
    noResults: 'Nu au fost găsite noduri compatibile'
  }
}