const host = window.location.host || 'ideascube.io';
const apiEndpoint = `http://api.${host}/process_id`;
const projectTexts = document.getElementsByClassName('project_text');
const projectNameInput = document.getElementById('projectName');
const processIdInput = document.getElementById('processId');
const messageElement = document.getElementById('message');
const deleteButton = document.getElementById('deleteBtn');
const editButton = document.getElementById('editBtn');
const checkboxElm = document.getElementById('iamsure');
const checkboxElm2 = document.getElementById('iamsure2');
const processIdRegex = /[0-9a-fA-F]{32}/

function handleCheckboxChange() {
    const isChecked = checkboxElm.checked;
    if(isChecked === true) {
        for(const text of projectTexts) {
            text.style.color = 'black';
        }
        projectNameInput.removeAttribute('disabled');
        processIdInput.removeAttribute('disabled');
        editButton.removeAttribute('disabled');
    } else {
        for(const text of projectTexts) {
            text.style.color = 'grey';
        }
        projectNameInput.setAttribute('disabled', 'disabled');
        processIdInput.setAttribute('disabled', 'disabled');
        editButton.setAttribute('disabled', 'disabled');
    }
}

function handleCheckboxChange2() {
    const isChecked = checkboxElm2.checked;
    if(isChecked === true) {
        deleteButton.removeAttribute('disabled');
    } else {
        deleteButton.setAttribute('disabled', 'disabled');
    }
}


function handleMessage(message, error = true) {
    if(error) {
        messageElement.classList.remove('message-success');
        messageElement.classList.add('message-error');
    } else {
        messageElement.classList.remove('message-error');
        messageElement.classList.add('message-success');
    }
    messageElement.innerHTML = message;
}

async function getProjectReference() {
    const error = 'Error while getting project reference';
    try {
        const result = await fetch(apiEndpoint);
        if(result.ok) {
            const json = await result.json();
            if(!json || json.process_id === undefined || json.project_name === undefined) {
                return handleMessage(error);
            } else {
                projectNameInput.value = json.project_name;
                processIdInput.value = json.process_id;
            }
        } else {
            return handleMessage(error);
        }
    } catch {
        return handleMessage(error);
    }
}

async function putProjetReference() {
    const confirmation = confirm(`WARNING !\nYou are editing project reference.\nPlease confirm that you know what you are doing :`);
    if(!confirmation) return;
    const error = 'Error while editing project reference';
    const projectName = projectNameInput.value;
    const processId = processIdInput.value;

    if(projectName === '') {
        return handleMessage('Project name should not be empty !')
    }

    if(processId === '') {
        return handleMessage('Process ID should not be empty !')
    }

    if(!processIdRegex.test(processId)) {
        return handleMessage('Invalid process ID !')
    }

    const data = {
        project_name: projectName,
        process_id: processId,
    };

    try {
        const result = await fetch(apiEndpoint, {
            method: 'put',
            body: JSON.stringify(data),
            headers: { "Content-Type": "application/json" },
        });
        if(result.ok) {
            handleMessage('Project reference edited !', false)
            await getProjectReference();
        } else {
            return handleMessage(error);
        }
    } catch {
        return handleMessage(error);
    }
}

async function deleteProjectReference() {
    const confirmation = confirm(`WARNING !\nYou asked to reset the project reference.\nPlease confirm that you know what you are doing :`);
    if(!confirmation) return;
    const error = 'Error while deleting project reference';
    try {
        const result = await fetch(apiEndpoint, { method: 'delete' })
        if(result.ok) {
            handleMessage('Project reference deleted !', false)
            await getProjectReference();
        } else {
            return handleMessage(error);
        }
    } catch {
        return handleMessage(error);
    }
}

getProjectReference()