import * as mutationTypes from './mutation_types'
import _ from 'lodash'

let listedCategoriesMutation = mutationTypes.LISTED_CATEGORIES
let listedTermsMutation = mutationTypes.LISTED_TERMS
let categoryCreatedMutation = mutationTypes.CATEGORY_CREATED
let categoryToEditLoadedMutation = mutationTypes.CATEGORY_TO_EDIT_LOADED
let categoryUpdatedMutation = mutationTypes.CATEGORY_UPDATED
let categoryDeletedMutation = mutationTypes.CATEGORY_DELETED
let termCreatedMutation = mutationTypes.TERM_CREATED
let termDeletedMutation = mutationTypes.TERM_DELETED
let categoryThumbnailUpdated = mutationTypes.CATEGORY_THUMBNAIL_UPDATED
let replacedCategory = mutationTypes.REPLACED_CATEGORIES

let categoryMapping = u => {
  let newObj = _.omit(u, 'links')
  newObj.detailUrl = _.find(u.links, l => l.rel === 'self').href
  newObj.thumbnailUrl = _.find(u.links, l => l.rel === 'thumbnail').href
  return newObj
}

let termMapping = t => {
  let newObj = _.omit(t, 'links')
  newObj.detailUrl = _.find(t.links, l => l.rel === 'self').href
  return newObj
}

export default {
  [replacedCategory]: (state, {categories}) => {
    let newObject = _.cloneDeep(categories)
    state.categories = newObject
  },
  [listedCategoriesMutation]: (state, {categories}) => {
    state.categories = _.map(categories, categoryMapping)
  },

  [listedTermsMutation]: (state, {terms}) => {
    state.terms = _.map(terms, termMapping)
  },

  [categoryCreatedMutation]: (state, {category}) => {
    state.categories.push(categoryMapping(category))
  },

  [categoryToEditLoadedMutation]: (state, {category}) => {
    state.edited_category = categoryMapping(category)
  },

  [categoryToEditLoadedMutation]: (state, {category}) => {
    state.edited_category = categoryMapping(category)
  },

  [categoryUpdatedMutation]: (state, {categoryIndex, category}) => {
    state.categories[categoryIndex] = categoryMapping(category)
  },

  [categoryDeletedMutation]: (state, {categoryIndex}) => {
    state.categories.splice(categoryIndex, 1)
  },

  [termCreatedMutation]: (state, {term}) => {
    state.terms.push(termMapping(term))
  },

  [termDeletedMutation]: (state, {term}) => {
    state.terms = _.reject(state.terms, t => t.term === term)
  },

  [categoryThumbnailUpdated]: (state, {newPictureSuffix}) => {
    state.pictureSuffix = newPictureSuffix
  }
}
