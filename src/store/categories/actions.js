import injector from 'vue-inject'
import * as mutationTypes from './mutation_types'
import _ from 'lodash'

let encase = injector.encase.bind(injector)

export const listCategories = encase(['backendApi'], (backendApi) => async ({commit}) => {
  let categories = await backendApi.listCategories()

  commit(mutationTypes.LISTED_CATEGORIES, {categories: categories})
})

export const saveCategory = encase(['backendApi'], (backendApi) => async ({commit, state}, {loadedCategoryIndex, labels, tags, playlists, applications, contents, directLinks}) => {
  let data = {
    labels,
    tags,
    playlists,
    applications,
    contents,
    directLinks
  }

  if (loadedCategoryIndex) {
    let categoryToUpdate = state.categories[loadedCategoryIndex]
    let category = await backendApi.updateCategory(categoryToUpdate.detailUrl, data)

    commit(mutationTypes.CATEGORY_UPDATED, {categoryIndex: loadedCategoryIndex, category: category})
  } else {
    let category = await backendApi.createCategory(data)

    commit(mutationTypes.CATEGORY_CREATED, {category})
  }
})

export const loadCategoryToEdit = encase(['backendApi'], (backendApi) => async ({commit, state}, {categoryIndex}) => {
  let categoryToLoad = state.categories[categoryIndex]

  let detailUrl = categoryToLoad.detailUrl

  let category = await backendApi.getCategory(detailUrl)

  commit(mutationTypes.CATEGORY_TO_EDIT_LOADED, {category: category})
})

export const deleteCategory = encase(['backendApi'], (backendApi) => async ({commit, state}, {categoryIndex}) => {
  await backendApi.deleteCategory(state.categories[categoryIndex].id)
  commit(mutationTypes.CATEGORY_DELETED, {categoryIndex})
})

export const setThumbnail = encase(['backendApi'], (backendApi) => async ({commit, state}, {categoryIndex, file, progress}) => {
  let categoryToUpdate = state.categories[categoryIndex]

  let thumbnailUrl = categoryToUpdate.thumbnailUrl

  await backendApi.updateThumbnail(thumbnailUrl, file.type, file.size, file, progress)

  let newPictureSuffix = Math.random()

  commit(mutationTypes.CATEGORY_THUMBNAIL_UPDATED, {newPictureSuffix})

  return file
})

export const getCategoryLinkIconUrl = encase(['backendApi'], (backendApi) => async ({commit, state}, {categoryId, directLinkIndex}) => {
  let url = await backendApi.getCategoryLinkIconUrl(categoryId, directLinkIndex)
  return url
})

export const deleteCategoryIcon = encase(['backendApi'], (backendApi) => async ({commit, state}, {categoryId, directLinkIndex}) => {
  await backendApi.deleteCategoryLinkIcon(categoryId, directLinkIndex)
})

export const setDirectLinkIcon = encase(['backendApi'], (backendApi) => async ({commit, state}, {categoryId, directLinkIndex, file, progress}) => {
  let thumbnailUrl = await backendApi.getCategoryLinkIconUrl(categoryId, directLinkIndex)
  await backendApi.updateThumbnail(thumbnailUrl, file.type, file.size, file, progress)
  return file
})

function updateAllCategory (backendApi, commit, categoriesBis) {
  let promises = []
  var counter = 0
  categoriesBis.forEach(async a => {
    let cnt = counter++

    let p = await backendApi.setCategoryDisplaySettings(a.id, cnt)

    promises.push(p)
  })
  Promise.all(promises).then(() => {
    commit(mutationTypes.REPLACED_CATEGORIES, {categories: categoriesBis})
  })
}

export const moveCategoryUp = encase(['backendApi'], (backendApi) => ({commit, state}, categoryIndex) => {
  if (categoryIndex === 0) {
    return
  }

  let currentCategory = state.categories[categoryIndex]
  let previousCategory = state.categories[categoryIndex - 1]
  var categoriesBis = _.cloneDeep(state.categories)

  categoriesBis[categoryIndex] = previousCategory
  categoriesBis[categoryIndex - 1] = currentCategory
  updateAllCategory(backendApi, commit, categoriesBis)
})

export const moveCategoryDown = encase(['backendApi'], (backendApi) => ({commit, state}, categoryIndex) => {
  if (categoryIndex >= state.categories.length - 1) {
    return
  }

  let currentCategory = state.categories[categoryIndex]

  let nextCategory = state.categories[categoryIndex + 1]

  // invert position in the array
  var categoriesBis = _.cloneDeep(state.categories)

  categoriesBis[categoryIndex] = nextCategory
  categoriesBis[categoryIndex + 1] = currentCategory
  updateAllCategory(backendApi, commit, categoriesBis)
})
