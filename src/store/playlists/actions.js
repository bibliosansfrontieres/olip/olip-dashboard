import injector from 'vue-inject'
import * as mutationTypes from './mutation_types'
import _ from 'lodash'

let encase = injector.encase.bind(injector)

export const listPlaylists = encase(['backendApi'], (backendApi) => async ({commit}) => {
  let playlists = await backendApi.listPlaylists()
  // console.log(playlists)

  commit(mutationTypes.LISTED_PLAYLISTS, {playlists: playlists})
})

export const savePlaylist = encase(['backendApi'], (backendApi) => async ({commit, state}, {loadedPlaylistIndex, title, pinned, user, applications, contents, directLinks}) => {
  let data = {
    title,
    user,
    applications,
    contents,
    directLinks,
    pinned
  }

  if (loadedPlaylistIndex) {
    let playlistToUpdate = state.playlists[loadedPlaylistIndex]
    let playlist = await backendApi.updatePlaylist(playlistToUpdate.detailUrl, data)
    commit(mutationTypes.PLAYLIST_UPDATED, {playlistIndex: loadedPlaylistIndex, playlist: playlist})
  } else {
    let playlist = await backendApi.createPlaylist(data)
    commit(mutationTypes.PLAYLIST_CREATED, playlist)
  }
})

export const loadPlaylistToEdit = encase(['backendApi'], (backendApi) => async ({commit, state}, {playlistIndex}) => {
  let playlistToLoad = state.playlists[playlistIndex]

  let detailUrl = playlistToLoad.detailUrl

  let playlist = await backendApi.getPlaylist(detailUrl)

  commit(mutationTypes.PLAYLIST_TO_EDIT_LOADED, {playlist: playlist})
})

export const deletePlaylist = encase(['backendApi'], (backendApi) => async ({commit, state}, {playlistIndex}) => {
  await backendApi.deletePlaylist(state.playlists[playlistIndex].id)
  commit(mutationTypes.PLAYLIST_DELETED, {playlistIndex})
})

export const setThumbnail = encase(['backendApi'], (backendApi) => async ({commit, state}, {playlistIndex, file, progress}) => {
  let playlistToUpdate = state.playlists[playlistIndex]
  let thumbnailUrl = playlistToUpdate.thumbnailUrl
  await backendApi.updateThumbnail(thumbnailUrl, file.type, file.size, file, progress)

  let newPictureSuffix = Math.random()

  commit(mutationTypes.PLAYLIST_THUMBNAIL_UPDATED, {newPictureSuffix})
  return file
})

export const getPlaylistLinkIconUrl = encase(['backendApi'], (backendApi) => async ({commit, state}, {playlistId, directLinkIndex}) => {
  let url = await backendApi.getPlaylistLinkIconUrl(playlistId, directLinkIndex)
  return url
})

export const deletePlaylistIcon = encase(['backendApi'], (backendApi) => async ({commit, state}, {playlistId, directLinkIndex}) => {
  await backendApi.deletePlaylistLinkIcon(playlistId, directLinkIndex)
})

export const setDirectLinkIcon = encase(['backendApi'], (backendApi) => async ({commit, state}, {playlistId, directLinkIndex, file, progress}) => {
  let thumbnailUrl = await backendApi.getPlaylistLinkIconUrl(playlistId, directLinkIndex)
  await backendApi.updateThumbnail(thumbnailUrl, file.type, file.size, file, progress)
  return file
})

function updateAllPlaylist (backendApi, commit, playlistsBis) {
  let promises = []
  var counter = 0
  // // reorder everything
  playlistsBis.forEach(async a => {
    let cnt = counter++

    let p = await backendApi.setPlaylistDisplaySettings(a.id, cnt)

    promises.push(p)
  })
  Promise.all(promises).then(() => {
    commit(mutationTypes.REPLACED_PLAYLISTS, {playlists: playlistsBis})
  })
}

export const movePlaylistUp = encase(['backendApi'], (backendApi) => ({commit, state}, playlistIndex) => {
  if (playlistIndex === 0) {
    return
  }

  let currentPlaylist = state.playlists[playlistIndex]
  let previousPlaylist = state.playlists[playlistIndex - 1]
  // invert position in the array
  var playlistsBis = _.cloneDeep(state.playlists)

  playlistsBis[playlistIndex] = previousPlaylist
  playlistsBis[playlistIndex - 1] = currentPlaylist

  updateAllPlaylist(backendApi, commit, playlistsBis)
})

export const movePlaylistDown = encase(['backendApi'], (backendApi) => ({commit, state}, playlistIndex) => {
  if (playlistIndex >= state.playlists.length - 1) {
    return
  }

  let currentPlaylist = state.playlists[playlistIndex]

  let nextPlaylist = state.playlists[playlistIndex + 1]

  // invert position in the array
  var playlistsBis = _.cloneDeep(state.playlists)

  playlistsBis[playlistIndex] = nextPlaylist
  playlistsBis[playlistIndex + 1] = currentPlaylist

  updateAllPlaylist(backendApi, commit, playlistsBis)
})
