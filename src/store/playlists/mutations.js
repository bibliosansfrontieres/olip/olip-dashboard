import * as mutationTypes from './mutation_types'
import _ from 'lodash'

let listedPlaylistsMutation = mutationTypes.LISTED_PLAYLISTS
let playlistCreatedMutation = mutationTypes.PLAYLIST_CREATED
let playlistToEditLoadedMutation = mutationTypes.PLAYLIST_TO_EDIT_LOADED
let playlistUpdatedMutation = mutationTypes.PLAYLIST_UPDATED
let playlistDeletedMutation = mutationTypes.PLAYLIST_DELETED
let playlistThumbnailUpdated = mutationTypes.PLAYLIST_THUMBNAIL_UPDATED
let replacedPlaylist = mutationTypes.REPLACED_PLAYLISTS

let playlistMapping = u => {
  let newObj = _.omit(u, 'links')
  newObj.detailUrl = _.find(u.links, l => l.rel === 'self').href
  newObj.thumbnailUrl = _.find(u.links, l => l.rel === 'self').href + '/thumbnail'

  return newObj
}

export default {
  [replacedPlaylist]: (state, {playlists}) => {
    let newObject = _.cloneDeep(playlists)
    state.playlists = newObject
  },
  [listedPlaylistsMutation]: (state, {playlists}) => {
    state.playlists = _.map(playlists, playlistMapping)
  },

  [playlistCreatedMutation]: (state, playlist) => {
    state.playlists.push(playlistMapping(playlist))
  },

  [playlistToEditLoadedMutation]: (state, {playlist}) => {
    state.edited_playlist = playlistMapping(playlist)
  },

  [playlistUpdatedMutation]: (state, {playlistIndex, playlist}) => {
    state.playlists[playlistIndex] = playlistMapping(playlist)
  },

  [playlistDeletedMutation]: (state, {playlistIndex}) => {
    state.playlists.splice(playlistIndex, 1)
  },

  [playlistThumbnailUpdated]: (state, {newPictureSuffix}) => {
    state.pictureSuffix = newPictureSuffix
  }
}
