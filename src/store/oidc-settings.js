import config from '../tools/config'

var redUri = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '')

let getOidcSettings = async () => {
  let conf = await config()

  return {
    authority: conf.api_url + '/',
    clientId: 'dashboard',
    redirectUri: redUri + '/oidc-callback',
    responseType: 'id_token token',
    scope: 'openid profile',
    automaticSilentRenew: true,
    silentRedirectUri: redUri + '/silent-renew'
  }
}
export default getOidcSettings
