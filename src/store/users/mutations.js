import * as mutationTypes from './mutation_types'
import _ from 'lodash'

let listedUsersMutation = mutationTypes.LISTED_USERS
let userAddedMutation = mutationTypes.USER_ADDED
let userRemovedMutation = mutationTypes.USER_REMOVED

export default {
  [listedUsersMutation]: (state, {users}) => {
    state.users = _.map(users, u => {
      let usr = {
        username: u.username,
        name: u.name
      }

      if (u.admin) {
        usr.admin = u.admin
      }

      if (u.provider) {
        usr.provider = u.provider
      }

      return usr
    })
  },

  [userAddedMutation]: (state, {user}) => {
    let newUsers = _.cloneDeep(state.users)

    let existingUser = _.find(newUsers, u => u.username === user.username)

    if (!existingUser) {
      existingUser = {}
      newUsers.push(existingUser)
    }

    _.assign(existingUser, _.pick(user, 'name', 'username', 'admin'))

    state.users = newUsers
  },

  [userRemovedMutation]: (state, {username}) => {
    let newUsers = _.cloneDeep(state.users)

    _.remove(newUsers, u => u.username === username)

    state.users = newUsers
  }
}
