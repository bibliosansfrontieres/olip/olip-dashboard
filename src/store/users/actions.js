import injector from 'vue-inject'
import * as mutationTypes from './mutation_types'

let encase = injector.encase.bind(injector)

export const listUsers = encase(['backendApi'], (backendApi) => async ({commit}) => {
  let users = await backendApi.listUsers()

  commit(mutationTypes.LISTED_USERS, {users: users})
})

export const saveUser = encase(['backendApi'], (backendApi) => async ({commit}, userdata) => {
  let data = {
    name: userdata.name,
    admin: userdata.admin
  }

  if (userdata.password) {
    data.password = userdata.password
  }

  let username = userdata.username

  let usr = await backendApi.saveUser(username, data)

  commit(mutationTypes.USER_ADDED, {user: usr})
})

export const deleteUser = encase(['backendApi'], (backendApi) => async ({commit}, userdata) => {
  let username = userdata.username

  await backendApi.deleteUser(username)

  commit(mutationTypes.USER_REMOVED, {username})
})
