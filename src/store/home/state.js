export default {
  endpoints: [],
  categoryEndpoints: [],
  playlistEndpoints: [],
  playlistsInCategory: [],
  visiblePlaylists: [],
  logos: [],
  pictureSuffix: ''
}
