import * as mutationTypes from './mutation_types'
import _ from 'lodash'

let reloadingEndpointsMutation = mutationTypes.RELOADING_ENDPOINTS
let listedEndpointsMutation = mutationTypes.LISTED_ENDPOINTS
let reloadingCategoryEndpointsMutation = mutationTypes.RELOADING_CATEGORY_ENDPOINTS
let listedCategoryEndpointsMutation = mutationTypes.LISTED_CATEGORY_ENDPOINTS
let listedPlaylistInCategory = mutationTypes.LISTED_PLAYLISTS_IN_CATEGORY
let reloadingPlaylistEndpointsMutation = mutationTypes.RELOADING_PLAYLIST_ENDPOINTS
let listedPlaylistEndpointsMutation = mutationTypes.LISTED_PLAYLIST_ENDPOINTS
let listedVisiblePlaylists = mutationTypes.LISTED_VISIBLE_PLAYLIST
let logoThumbnailUpdated = mutationTypes.LOGO_THUMBNAIL_UPDATED
let logothumbnailgot = mutationTypes.LOGO_THUMBNAIL_GOT

let playlistMapping = u => {
  let newObj = _.omit(u, 'links')
  newObj.detailUrl = _.find(u.links, l => l.rel === 'self').href
  return newObj
}

export default {
  [listedEndpointsMutation]: (state, {endpoints}) => {
    state.endpoints = state.endpoints.concat(endpoints)
  },

  [reloadingEndpointsMutation]: (state) => {
    state.endpoints.length = 0
  },

  [listedCategoryEndpointsMutation]: (state, {endpoints}) => {
    state.categoryEndpoints = state.categoryEndpoints.concat(endpoints)
  },

  [reloadingCategoryEndpointsMutation]: (state) => {
    state.categoryEndpoints.length = 0
  },

  [listedPlaylistInCategory]: (state, {playlists}) => {
    state.playlistsInCategory = _.map(playlists, playlistMapping)
  },

  [reloadingPlaylistEndpointsMutation]: (state) => {
    state.playlistEndpoints.length = 0
  },

  [listedPlaylistEndpointsMutation]: (state, {endpoints}) => {
    state.playlistEndpoints = state.playlistEndpoints.concat(endpoints)
  },

  [listedVisiblePlaylists]: (state, {playlists}) => {
    state.visiblePlaylists = _.map(playlists, playlistMapping)
  },

  [logoThumbnailUpdated]: (state, {newPictureSuffix}) => {
    state.pictureSuffix = newPictureSuffix
  },

  [logothumbnailgot]: (state, {logoThumbnail}) => {
    state.logos = logoThumbnail
  }
}
