import injector from 'vue-inject'
import * as mutationTypes from './mutation_types'
import _ from 'lodash'

let encase = injector.encase.bind(injector)

function _getAllEndpointsForApps (backendApi, applications, options) {
  let promises = []

  applications.forEach(a => {
    // find endpoints url
    let endpointsUrl = _.find(a.links, l => l.rel === 'endpoints').href

    // get the picture url as well
    let pictureUrl = _.find(a.links, l => l.rel === 'picture').href

    var promise

    if (!options) {
      options = {}
    }

    let forContent = options.forContent

    if (forContent) {
      promise = backendApi.listApplicationEndpoints(endpointsUrl, {withContainers: false, withContents: true, ...options})
    } else {
      promise = backendApi.listApplicationEndpoints(endpointsUrl, options)
    }

    promises.push(promise.then(endpoints => {
      // add the picture of the app to each container
      endpoints.forEach(d => {
        d.picture = pictureUrl
      })
      return endpoints
    }))
  })
  return promises
}

async function _listEndpointsforBundles (backendApi, commit, mutationType, allApps, bundles, options, directLinks) {
  // filter out the list of retrieved applications with the one we are interested in for the category
  let visibleApplicationsInCategory = _.filter(allApps, a => bundles.includes(a.bundle))

  // we now have all the bundles. Trigger the request to retrieve the endpoints
  let promises = _getAllEndpointsForApps(backendApi, visibleApplicationsInCategory, options)

  // add direct links endpoints as well
  if (directLinks) {
    directLinks.forEach(link => {
      let promise = (options.linksFor === 'category') ? backendApi.getCategoryLinkIconUrl(link.category_id, link.index) : backendApi.getPlaylistLinkIconUrl(link.playlist_id, link.index)
      promises.push(promise.then(pictureUrl => {
        return {'name': link.title, 'url': link.href, 'picture': pictureUrl}
      }))
    })
  }

  // wait for all response to come back
  let responses = await Promise.all(promises)

  let allEndpoints = []

  // we have an array containing in each item an array of endpoints. Flatten everything
  allEndpoints = _.flatten(responses)

  commit(mutationType, {endpoints: allEndpoints})
}

export const listEndpoints = encase(['backendApi'], (backendApi) => async ({commit}) => {
  commit(mutationTypes.RELOADING_ENDPOINTS)

  let applications = await backendApi.listApplications('installed', true)

  let promises = _getAllEndpointsForApps(backendApi, applications)

  // wait for all response to come back
  let responses = await Promise.all(promises)

  let allEndpoints = []

  // we have an array containing in each item an array of endpoints. Flatten everything
  allEndpoints = _.flatten(responses)

  commit(mutationTypes.LISTED_ENDPOINTS, {endpoints: allEndpoints})
})

export const listCategoryEndpoints = encase(['backendApi'], (backendApi) => async ({commit, rootState}, {categoryIndex}) => {
  commit(mutationTypes.RELOADING_CATEGORY_ENDPOINTS)

  let category = rootState.categories.categories[categoryIndex]

  // get the category details
  let detailedCategory = await backendApi.getCategory(category.detailUrl)

  // we have a list of bundles. Recover all applications so that we can get the details from them
  // set to TRUE or FALSE, the visibility selector will act on application display
  let allApps = await backendApi.listApplications('installed')

  // get all endpoints for standard application inclusion
  let applications = detailedCategory.applications

  let categoryId = category.id

  if (applications == null) {
    applications = []
  }

  let options = {categoryId, linksFor: 'category'}

  if (detailedCategory.contents) {
    let contentBundles = _.uniq(_.map(detailedCategory.contents, c => c.bundle))
    contentBundles.forEach(async b => {
      options.forContent = true
      applications.push(b)
    })
  }

  await _listEndpointsforBundles(backendApi, commit, mutationTypes.LISTED_CATEGORY_ENDPOINTS, allApps, applications, options, detailedCategory.direct_links)
})

export const listPlaylistEndpoints = encase(['backendApi'], (backendApi) => async ({commit, state}, {playlistIndex, fromCategory}) => {
  commit(mutationTypes.RELOADING_PLAYLIST_ENDPOINTS)

  let playlist
  if (fromCategory) {
    playlist = state.playlistsInCategory[playlistIndex]
  } else {
    playlist = state.visiblePlaylists[playlistIndex]
  }

  let playlistId = playlist.id

  // get the category details
  let detailedPlaylist = await backendApi.getPlaylist(playlist.detailUrl)
  // we have a list of bundles. Recover all applications so that we can get the details from them
  // set to TRUE or FALSE, the visibility selector will act on application display
  let allApps = await backendApi.listApplications('installed')

  // get all endpoints for standard application inclusion
  let applications = detailedPlaylist.applications

  if (applications == null) {
    applications = []
  }

  let options = {playlistId, linksFor: 'playlist'}

  if (detailedPlaylist.contents) {
    let contentBundles = _.uniq(_.map(detailedPlaylist.contents, c => c.bundle))
    contentBundles.forEach(async b => {
      options.forContent = true
      applications.push(b)
    })
  }

  await _listEndpointsforBundles(backendApi, commit, mutationTypes.LISTED_PLAYLIST_ENDPOINTS, allApps, applications, options, detailedPlaylist.direct_links)
})

export const listPlaylistInCategory = encase(['backendApi'], (backendApi) => async ({commit, rootState}, {categoryIndex}) => {
  let category = rootState.categories.categories[categoryIndex]

  let playlists = await backendApi.listPlaylists(category.id)

  commit(mutationTypes.LISTED_PLAYLISTS_IN_CATEGORY, {playlists: playlists})
})

export const listVisiblePlaylists = encase(['backendApi'], (backendApi) => async ({commit, rootState}) => {
  let currentUser = _.get(rootState, 'oidc.user.sub', 'anonymous')

  let playlists = await backendApi.listPlaylists(null, currentUser)

  commit(mutationTypes.LISTED_VISIBLE_PLAYLIST, {playlists: playlists})
})

export const getAdminLogo = encase(['backendApi'], (backendApi) => async ({commit}) => {
  let logoThumbnail = await backendApi.getAdminLogo()
  commit(mutationTypes.LOGO_THUMBNAIL_GOT, {logoThumbnail: logoThumbnail})
})

export const setAdminLogo = encase(['backendApi'], (backendApi) => async ({commit, state}, {file, progress}) => {
  await backendApi.setAdminLogo(file.type, file.size, file, progress)

  let newPictureSuffix = Math.random()

  commit(mutationTypes.LOGO_THUMBNAIL_UPDATED, {newPictureSuffix})
  return file
})
