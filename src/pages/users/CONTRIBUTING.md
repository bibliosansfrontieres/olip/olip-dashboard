# How to create a new user and update a logo

To create a new logo / user, you should first login as an admin on Olip.
and after click on user in the nav panel on your left.

## Create a user

Olip Dashboard will redirect you on the following page : `pages/users/list` and click on the `Add User` button:

```html
<q-btn :label="$t('Save')"
     @click="save"
     outline
     rounded
    class="action-btn full-width"
    v-if="this.isEditing"
  />
```
This button call the `add()` fonction 

```html
add: function () {
    this.cleanModal()
    this.layoutModal = true
    }
```
to open the modal form fro creating the user:

```html
 <q-modal v-model="layoutModal"  class="user"  :content-css="{minWidth: '40vw', minHeight: '80vh'}">
  <q-modal-layout>
    <q-toolbar slot="footer" class="justify-end" inverted>
         <div class="col-3">
            <q-btn :label="$t('Cancel')" @click="closeModal" flat  rounded  class="full-width"  />
         </div>
              <div class="col-5">
                <q-btn :label="$t('Save')"
                       @click="save"
                       outline
                       rounded
                       class="action-btn full-width"
                       v-if="this.isEditing"
                />
                <q-btn :label="$t('user.add')"
                       @click="save"
                       outline
                       rounded
                       class="action-btn full-width"
                       v-else
                />
              </div>
            </q-toolbar>
            <div class="layout-padding">
              <div class="row title-row items-center margin-bottom-20">
                <i class="fas fa-user-plus"></i>
                <div v-if="this.isEditing" class="page-title">{{ $t('Editing') }} {{username}}</div>
                <div class="page-title" v-else>{{ $t('user.adding') }}</div>
              </div>
              <div class="row">
                <div class="col-12">
                  <q-field :helper="$t('user.helper')">
                    <q-input
                      v-model="username"
                      max-length="50"
                      :error-label="$t('user.helper_error')"
                      :error="$v.username.$error"
                      @blur="$v.username.$touch"
                      :float-label="$t('user.name')"
                      :disabled="isEditing"
                      :readonly="isEditing"
                    />
                  </q-field>
                  <q-field>
                    <q-input v-model="name"
                             max-length="250"
                             :float-label="$t('Name')"
                             :disabled="isEditing && provider!=null && provider != ''"
                             :readonly="isEditing && provider!=null && provider != ''"  />
                  </q-field>
                  <q-field>
                    <q-input type="password"
                             v-model="password"
                             max-length="50"
                             :float-label="$t('Password')"
                             v-if="!provider"
                    />
                  </q-field>
                  <q-field>
                    <q-checkbox v-model="admin" :label="$t('user.Admin')" left-label/>
                </q-field>
            </div>
        </div>
     </div>
  </q-modal-layout>
</q-modal>
```
 fill the form and click Add User button, to submit the form
 it calls the `save()` function 
 
```html
save: function () {
  if (this.$v.$invalid) {  return   }
      this.saveUser({
        name: this.name,
        username: this.username,
        admin: this.admin,
        password: this.password
      })
      this.closeModal()
    }
```
and here the `saveUser()` is called from user store actions

```js
export const saveUser = encase(['backendApi'], (backendApi) => async ({commit}, userdata) => {
  let data = {
    name: userdata.name,
    admin: userdata.admin
  }
  if (userdata.password) {
    data.password = userdata.password
  }
  let username = userdata.username
  let usr = await backendApi.saveUser(username, data)
  commit(mutationTypes.USER_ADDED, {user: usr})
})
```
and after an other `saveUser()` is called from the `backend_api.js` 

```js
async saveUser (username, user) {
    let config = await this.config()
    let req = await this.axios.put(config.api_url + '/users/' + username, user)
    return req.data
  }
```

and the user is created 👍️, good job.

## Update the user

To delete a user you should first be logged as an admin on Oilp.
and go to the user list view.
the carts will be displayed according to the number of users that exist, you just have to click on the edit button on the user card of you want to update .

```html
<q-btn outline rounded class="action-btn" @click="edit(user)"  >{{ $t('Edit') }}</q-btn>
```
just after clicking that, the `edit ()` function is called 

```js
edit: function (user) {
      this.name = user.name
      this.username = user.username
      this.provider = user.provider
      this.admin = !!user.admin
      this.isEditing = true
      this.layoutModal = true
    }
```
this function open the user form modal filled with the user data selected.
now you can update data and and save the form with the following button:

```html
<q-btn :label="$t('Save')"
   @click="save"
   outline
   rounded
   class="action-btn full-width"
   v-if="this.isEditing"
  />
```
from here the rest is the same as creating a user.
## Delete the user

To delete a user you should first be logged as an admin on Olip.
and go to the user list view.
the carts will be displayed according to the number of users that exist, you just have to click on the delete button on the user card you want to delete.

```html
 <q-btn outline rounded :disable="user.username === 'admin'"
    class="action-btn" @click="deleteUser(user)" >{{ $t('Delete') }}</q-btn>
```
when this button is clicked, the `deleteUser()` is called from the user store action.

```js
export const deleteUser = encase(['backendApi'], (backendApi) => async ({commit}, userdata) => {
  let username = userdata.username
  await backendApi.deleteUser(username)
  commit(mutationTypes.USER_REMOVED, {username})
})
```
and here, an other `deleteUser()` function is called from the backend api to update the database:

```js
async deleteUser (username) {
    let config = await this.config()
    await this.axios.delete(config.api_url + '/users/' + username)
  }
```
## Upload a logo

⚠️the source code of this part is written on this page :  `src/layouts/default` ⚠️

First you have to hover the logo the with cursor and click on the button  that appears
```html
<q-btn
    :title="$t('Upload_logo')"
     class="spacing"
     size="sm"
     round
     color="orange"
     icon="image"
     @click.native="displayThumbnailInDialog()"
 />
```

It will lunch the modal with everything you need to download a new logo.

```html
 <q-dialog v-model="uploadProfileImageDialoddiplayed" :ok=false >
      <span slot="title" class="row items-center">
        <div class="text-h6">{{ $t('Upload Profile image') }}</div>
        <q-btn icon="close" flat round dense size="sm" @click.native="closeUpload()"/>
      </span>
      <div slot="body" >
        <q-uploader :url="categoryThumbnailUrl" send-raw method="PUT"
        :upload-factory="uploadFile" max-file-size="500000" @uploaded="finishedUpload" @fail="uploadFailed"
        auto-expand/>
      </div>
 </q-dialog>
```
select a new logo and save it, the `uploadFile()` fucntion is called 
```html
 uploadFile: function (file, updateProgress) {
      let promise = this.setAdminLogo({file: file, progress: updateProgress})
      return promise
    }
```
in this function the `setAdminLogo()` function is called from the home store action.

```js
 export const setAdminLogo = encase(['backendApi'], (backendApi) => async ({commit, state}, {file, progress}) => {
  await backendApi.setAdminLogo(file.type, file.size, file, progress)
  let newPictureSuffix = Math.random()
  commit(mutationTypes.LOGO_THUMBNAIL_UPDATED, {newPictureSuffix})
  return file
})
```
and to finish it, the `setAdminLogo()` function is called from the backend api to to complete the operation:

```js
  async setAdminLogo (contentType, contentLength, content, progress) {
    let defaultConfig = await this.config()
    let logoThumbnailUrl = defaultConfig.api_url + '/logo/admin/thumbnail'
    const config = {
      onUploadProgress: progressEvent => {
        progress(progressEvent.loaded)
      },
      headers: {
        'Content-Type': contentType,
        'Content-Length': contentLength
      }
    }
    await this.axios.put(logoThumbnailUrl, content, config)
  }
```
and that's all!! 👏️
