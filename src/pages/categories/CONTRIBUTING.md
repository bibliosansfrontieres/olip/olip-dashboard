# How works category in olip dashboard

Category and tablet management is similar; so in this document we will only go through the management of the categories, as far as the playlists are concerned, you just have to follow the same procedure 

## Create a category

To create a new category, go to this page:
`pages/categories/lists` and click to the following add button: 
```js
 <q-btn
      icon="add"
      :label="$t('category.add')"
      @click="newCategory"
      class="action-btn"
      rounded
 />
```
it will drive you to the create category form on this link : `pages/categories/add`, fill this form and send it.
when the send category button is clicked on this page, `save()` function is automatically lunched 

```js
save: async function () {
      if (this.current_category.id && this.iconIndexesToDelete.length) {
        // user deleted those links so we need to clear their icons
        this.iconIndexesToDelete.forEach(async (index) => {
          await this.deleteCategoryIcon({categoryId: this.current_category.id, directLinkIndex: index})
        })
      }
      await this.saveCategory({
        loadedCategoryIndex: this.loadedCategoryIndex,
        labels: this.labels,
        playlists: this.playlists,
        tags: this.tags,
        applications: this.applications,
        contents: this.contents,
        directLinks: this.direct_links
      })
      this.$router.push('/categories')
    }
```
the above function, run `saveCtegory()` function from the store category actions. which is written in the  category action store

```js
export const saveCategory = encase(['backendApi'], (backendApi) => async ({commit, state}, {loadedCategoryIndex, labels, tags, playlists, applications, contents, directLinks}) => {
  let data = {
    labels,
    tags,
    playlists,
    applications,
    contents,
    directLinks
  }
  if (loadedCategoryIndex) {
    let categoryToUpdate = state.categories[loadedCategoryIndex]
    let category = await backendApi.updateCategory(categoryToUpdate.detailUrl, data)
    commit(mutationTypes.CATEGORY_UPDATED, {categoryIndex: loadedCategoryIndex, category: category})
  } else {
    let category = await backendApi.createCategory(data)
    commit(mutationTypes.CATEGORY_CREATED, {category})
  }
})
  } 
```
and to finish the processes, olip dasboard send the request to olip API, through the following function in backend api :

```js
 async updateCategory (categoryUrl, data) {
    let req = await this.axios.put(categoryUrl, data)
    return req.data
  }
```

now, go to the category list page, you will see a new line in the table containing your just created category. 😉️

## How to manage a category

in the listing category page, there is an actions column, which contain all the applicable actions on category, in fact you can :

- Edit a category
- Upload a new thumbnail
- Delete a category 

### Edit a category

Click on the edit button in category line you want to edit:

```html
 <q-btn
     :title="$t('Edite_category')"
      class="spacing"
      size="sm"
      round
      color="teal"
      icon="edit"
      @click.native="editCategory(props.row.__index)"
 />
```
As you can notice by clicking on this button, olip dashboard lunch the  `editCategory()` function, 

```js
 editCategory: function (categoryIndex) {
      console.log('index dans la page test2 fonction deletecat: ' + categoryIndex)
      console.log('id dans la page test2 fonction deletecat: ' + this.categories[categoryIndex].id)
      this.$router.push('/categories/edit/' + categoryIndex)
    },
```
After that you will see the edit view, displayed with a form pre-filled with data from the selected category on this page `pages/categories/lists` , this redirection is done on this line  `  this.$router.push('/categories/edit/' + categoryIndex)`

make your changes then press the save button, tha t will send your form to the api, by running the same function as during category creation. 🖖️

### Upload a new thubmail
To upload a category thumbnail, got to the category list view, here ` pages/categories/lists`, on the table displayed click on the upload button see below his code : 
```html
  <q-btn
     :title="$t('Upload_thumbnail')"
     class="spacing"
     size="sm"
     round
     color="orange"
     con="image"
    @click.native="uploadThumbnail(props.row.__index)"
/>
```

As you can notice, when you click on  this button, olip dashboard lunch the  `uploadThumbnail()` function:

```js
uploadThumbnail: function (categoryIndex) {
      this.categoryThumbnailUrl = this.categories[categoryIndex].thumbnailUrl
      this.loadedCategoryIndex = categoryIndex
      this.uploadDialogDisplayed = true
    },
```
This function open a modal which allow to select a new thumbnail, see below the source code of this model:
```html
<q-dialog v-model="uploadDialogDisplayed" :ok=false>
      <span slot="title" class="row items-center">
        <div class="text-h6">{{ $t('Upload_thumbnail') }}</div>
        <q-btn icon="close" flat round dense size="sm" @click.native="closeUpload()"/>
      </span>
      <div slot="body">
        <q-uploader :url="categoryThumbnailUrl" send-raw method="PUT"
        :upload-factory="uploadFile" max-file-size="500000" @uploaded="finishedUpload" @fail="uploadFailed"
        auto-expand/>
      </div>
    </q-dialog>
```

select an image and save it with the ☁️ button, then the upload file function is triggered

```js
uploadFile: function (file, updateProgress) {
      let promise = this.setThumbnail({categoryIndex: this.loadedCategoryIndex, file: file, progress: updateProgress})
      return promise
    },
```
to send the select file the database, by first calling the `setThumbnail()`
file function which is written in the category actions store

```js
export const setThumbnail = encase(['backendApi'], (backendApi) => async ({commit, state}, {categoryIndex, file, progress}) => {
  let categoryToUpdate = state.categories[categoryIndex]
  let thumbnailUrl = categoryToUpdate.thumbnailUrl
  await backendApi.updateThumbnail(thumbnailUrl, file.type, file.size, file, progress)
  let newPictureSuffix = Math.random()
  commit(mutationTypes.CATEGORY_THUMBNAIL_UPDATED, {newPictureSuffix})
  return file
})
```
and to finaly send the request to Olip Api through the following function written on the `backend_api file` 

```js
async updateThumbnail (categoryThumbnailUrl, contentType, contentLength, content, progress) {
    const config = {
      onUploadProgress: progressEvent => {
        progress(progressEvent.loaded)
      },
      headers: {
        'Content-Type': contentType,
        'Content-Length': contentLength
      }
    }
    await this.axios.put(categoryThumbnailUrl, content, config)
  }
```
and yes ✌️, you did it !

### Delete a category
To delete a category , got to the category list view, here `pages/categories/lists`, on the table displayed click on the delete button see below his code :


```js
<q-btn
    :title="$t('Delete_category')"
    class="spacing"
    size="sm"
    round
    color="red"
    icon="delete"
    @click.native="delCategory(props.row.__index)"
 />
```
As you can notice, when you click on  this button, olip dashboard lunch the  `delCategory()` function:

```js
delCategory: function (categoryIndex) {
      this.$q.dialog({
        title: this.$t('category.Delete'),
        message: this.$t('category.delete_confirm'),
        ok: this.$t('state.Delete'),
        cancel: this.$t('Cancel')
      }).then(() => {
        this.$q.notify({
          message: this.$t('state.Deleted'),
          timeout: 3000,
          type: 'positive',
          icon: 'delete',
          detail: this.$t('category.delete_confirmation'),
          position: 'top-right'
        })
        this.deleteCategory({categoryIndex})
      })
    }
```
After that, the request is sent to the database,through  this function `deleteCategory()` in the category actions store.

```js
export const deleteCategoryIcon = encase(['backendApi'], (backendApi) => async ({commit, state}, {categoryId, directLinkIndex}) => {
  await backendApi.deleteCategoryLinkIcon(categoryId, directLinkIndex)
})
```
and to finish it, the `deleteCategoryLinkIcon()` fucntion is called from the backend api to to complete the action

```js
async deleteCategoryLinkIcon (categoryId, linkIndex) {
    let url = await this.getCategoryLinkIconUrl(categoryId, linkIndex)
    await this.axios.delete(url)
  }
```
Super! ㊗️ congratulation !
