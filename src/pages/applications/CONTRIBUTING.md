# How works applications in olip dashboard

## Download an application

The applications can be downloaded from a catalog available on catalog page, by clicking on one of the download buttons on the displayed application cards.

Here is the path that leads to the directory containing all the application management pages.

`src/pages/applications/applications`


For more information, see below the router file source code, which show routes to all application management pages

```js
 {
   path: '/applications',
   component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/applications/applications') },
      { path: ':bundle', component: () => import('pages/applications/application_details') },
      { path: ':bundle/contents', component: () => import('pages/applications/contents') },
      { path: ':bundle/configuration', component: () => import('pages/applications/configuration') }
    ]
 },
```
The whole route file is available  [here](https://gitlab.com/bibliosansfrontieres/olip/olip-dashboard/-/blob/master/src/router/routes.js)

 * *Add a route to new page*
 
To add a new route, use this pattern and add it in the export table on router file with good configuration.

```js
 {
    path: '/myparent_path',
    component: () => import('layouts/default'),
    children: [
      { path: '/mychild_path', component: () => import('mychild_component_path') }
    ]
  },
```
* *Create a component*


To add new component you should first create a path in the router file as seen above, and after create a vue js file according to the **mychild_component_path**
 
## Install an application
After downloading the application, install it from the table which displayed the downloaded application on this pages:  `src/pages/applications/installed_applications` 

 Click on the install button and see below what happens under the hood:

```html
 <q-btn size="sm"
  round
  dense
  color="secondary"
  icon="play_arrow"
  v-on:click="installApp(props.row.bundle)"
  class="q-mr-xs"
  :disabled="props.row.currentState != props.row.targetState"/>
```
When the button is clicked, the `installApp()` is run and, this one triggers `triggerAppInstallation` action.

```js
 installApp: function (bundle) {
  this.triggerAppInstallation(bundle)
  this.$q.notify({
  message: `Installation`,
  timeout: 3000,
  type: 'info',
  detail: this.$t('application.will_install'),
  position: 'top-right'
  })
 }
```

It is good to know that olip-dashboard works with a Vuex which is a state management pattern + library for Vue.js applications. it serves as a centralized store for all the components in an application, with rules ensuring that the state can only be mutated in a predictable fashion, if you want to know more about that see the  [vuex store configuration](https://quasar.dev/quasar-cli/vuex-store) on quasar doc.

Let's get back to our topic, take a glance at this directory to see all olip dashboard stores: `src/store/`. All store as `action.js` file where all where all actions on the concerned resource are written.

The `triggerAppInstallation` action is define in `src/store/application/actions.js` file, see below its code :

```js
 export const triggerAppInstallation = encase(['backendApi'], (backendApi) => async ({commit, state}, bundle) => {
	let app = _.find(state.downloaded_applications, a => a.bundle === bundle)
	await backendApi.setAppTargetState(app.targetStateUrl, 'installed')
	commit(mutationTypes.TARGET_STATE_CHANGED, {bundle: bundle, target_state: 'installed'})
})
```
As you can notice, this action first calls a function defined in `backend_api.js` to launch the installation from the api and persist the status change of the concerned application and update the application store too with the new stats, here is how is define the `setAppTargetState()` fonction in `backend_api.js`.

```js
 async setAppTargetState (appTargetStateUrl, targetState, targetVersion) {
  let state = {'target_state': targetState}
  if (targetVersion) { state.target_version = targetVersion}
  await this.axios.put(appTargetStateUrl, state)
 }
```
Congratulations,  your application has been installed with success 👏️

## Install an application content
Application available contents are listed [here](https://gitlab.com/bibliosansfrontieres/olip/olip-dashboard/-/blob/master/src/pages/applications/contents.vue), to access this,  you should click on 
the view content button on the [installed application page ](https://gitlab.com/bibliosansfrontieres/olip/olip-dashboard/-/blob/master/src/pages/applications/installed_applications.vue), below the bouton code/

```js
<q-btn size="sm"
 round dense
 :color="contentButtonColor(props.row.bundle)"
 icon="description"
 v-if="props.row.contentsLink!=null"
 v-on:click="viewContent(props.row.bundle)"
 class="q-mr-xs"
/>
```
This button will drive you to the selected application list view, and then you can check the desired content in the content view page to install it. 

Before going so deep in Analyses; lest's see first how is build this page:

There are 4 different tables on this page, each of them display contents in different states : __uninstalled, installed, upgradable and broken__, here is the source code the uninstalled content list table  :

```html
<q-table
 :title="$t('content.available')"
 :data="available_contents"
 :columns="columns"
 :visible-columns="visible_columns"
 :filter="filt"
 :filter-method="tableFilter"
 row-key="content_id"
 selection="multiple"
 :selected.sync="available_selected">
  <template slot="top-right" slot-scope="props">
   <q-select
     :stack-label="$t('Subject')"
     v-model="filt.subject"
     :options="available_subjects" 
   />
   <q-select
      :stack-label="$t('Language')"
      v-model="filt.language"
      :options="available_languages"
   />
   </template>
    <template slot="top-selection" slot-scope="props">
     <q-btn
       color="secondary"
       flat
       :label="$t('state.Install')"
       class="q-mr-sm"
       @click="installContents"
     />
  </template>
</q-table>
```
So on this page when the install button is clicked, olip dashboard select all the checked content in the table and call the `installContents()` function :

```js
installContents: function () {
      let contentIds = _.map(this.available_selected, e => e.content_id)
      this.triggerContentsTargetStateChange({bundle: this.$route.params.bundle, contentIds, newState: 'installed'})
      this.available_selected = []
    }
```
this fonction triggers the `triggerContentsTargetStateChange
`, to install the the content :

```js
export const triggerContentsTargetStateChange = encase(['backendApi'], (backendApi) => async ({commit, state}, {bundle, contentIds, newState}) => {
  let app = _.find(state.installed_applications, a => a.bundle === bundle)

  contentIds.forEach(async cId => {
    let content = _.find(app.contents, c => c.content_id === cId)
    let targetStateUrl = content.targetStateUrl
    let res = await backendApi.setContentTargetState(targetStateUrl, newState)
    if (typeof res.data.message !== 'undefined') {
      Notify.create({
        message: res.data.message
      })
    } else {
      commit(mutationTypes.CONTENT_INSTALLATION_STARTED, {bundle, cId, newState: newState})
    }
  })
})
```
and the conclude this process, the `backeg_api.js` file takes over and queries the api, through this function :

```js
async setContentTargetState (contentTargetState, targetState, targetVersion) {
 let state = {
  'target_state': targetState
 }
 if (targetVersion) {
  state.target_version = targetVersion
 }
 try {
  await this.axios.put(contentTargetState, state)
  return
 } catch (e) {
   return e.response
  }
}
```
And that 's all 🙂️!

## Uninstall an application content

In the installed content list table, check on the content to uninstall and click on the install button which appears on the left top corner of the table, see below the uninstall content table code source, all the triggered function called in the uninstall process:

* The table :

```html
<q-table
 :title="$t('content.installed')"
 :data="installed_contents"
 :columns="columns"
 :visible-columns="visible_columns"
 row-key="content_id"
 selection="multiple"
 :selected.sync="installed_selected">
 <template slot="top-selection" slot-scope="props">
  <q-btn
   color="secondary"
   flat
   :label="$t('state.Uninstall')"
   class="q-mr-sm"
   @click="uninstallContents"/>
  </template>
</q-table>
```

* The `uninstallContents()` function :

```js
  uninstallContents: function () {
      let contentIds = _.map(this.installed_selected, e => e.content_id)
      this.triggerContentsTargetStateChange({bundle: this.$route.params.bundle, contentIds, newState: 'uninstalled'})
      this.installed_selected = []
    },
```
 * The `triggerContentsTargetStateChange()` function :
 
```js
  export const triggerContentsTargetStateChange = encase(['backendApi'], (backendApi) => async ({commit, state}, {bundle, contentIds, newState}) => {
  let app = _.find(state.installed_applications, a => a.bundle === bundle)
  contentIds.forEach(async cId => {
    let content = _.find(app.contents, c => c.content_id === cId)
    let targetStateUrl = content.targetStateUrl
    let res = await backendApi.setContentTargetState(targetStateUrl, newState)
    if (typeof res.data.message !== 'undefined') {
      Notify.create({
        message: res.data.message
      })
    } else {
      commit(mutationTypes.CONTENT_INSTALLATION_STARTED, {bundle, cId, newState: newState})
    }
  })
})
```

* And to finish the call to the api through the `setContentTargetState()` function:

```js
async setContentTargetState (contentTargetState, targetState, targetVersion) {
    let state = {
      'target_state': targetState
    }
    if (targetVersion) {
      state.target_version = targetVersion
    }
    try {
      await this.axios.put(contentTargetState, state)
      return
    } catch (e) {
      return e.response
    }
  }
```
And it's done 🙂️!

## Update an application content

* The table:
```html
 <q-table
              :title="$t('content.upgradable')"
              :data="upgradable_contents"
              :columns="columns"
              :visible-columns="visible_columns"
              row-key="content_id"
              selection="multiple"
              :selected.sync="upgradable_selected"
            >
              <template slot="top-selection" slot-scope="props">
                <q-btn
                  color="secondary"
                  flat
                  :label="$t('state.Upgrade')"
                  class="q-mr-sm"
                  @click="upgradeContents"
                />
              </template>
            </q-table>
```
* The `upgradeContents()` function :

```js
upgradeContents: function () {
      let contentIds = _.map(this.upgradable_selected, e => e.content_id)
      this.triggerContentUpgrade({bundle: this.$route.params.bundle, contentIds})
      this.upgradable_selected = []
    },
```
* The `triggerContentUpgrade()` function :

```js
export const triggerContentUpgrade = encase(['backendApi'], (backendApi) => async ({commit, state}, {bundle, contentIds}) => {
  let app = _.find(state.installed_applications, a => a.bundle === bundle)

  contentIds.forEach(async cId => {
    let content = _.find(app.contents, c => c.content_id === cId)

    let targetStateUrl = content.targetStateUrl

    let currentState = content.targetState

    await backendApi.setContentTargetState(targetStateUrl, currentState, content.repositoryVersion)

    commit(mutationTypes.CONTENT_UPGRADED, {bundle, cId, targetVersion: content.repositoryVersion})
  })
})
```

* And to ficnish the call to the api through the `setContentTargetState()` function:

```js
async setContentTargetState (contentTargetState, targetState, targetVersion) {
    let state = {
      'target_state': targetState
    }

    if (targetVersion) {
      state.target_version = targetVersion
    }
    try {
      await this.axios.put(contentTargetState, state)
      return
    } catch (e) {
      return e.response
    }
  }
```
congratulations !!!!! 
## Reinstall an application content

The process is similar to the operations described above, see below the table :

```html
 <q-table
              :title="$t('content.broken')"
              :data="broken_contents"
              :columns="columns"
              :visible-columns="visible_columns"
              row-key="content_id"
              selection="multiple"
              :selected.sync="broken_selected"
            >
              <template slot="top-selection" slot-scope="props">
                <q-btn
                  color="secondary"
                  flat
                  :label="$t('content.reinstall')"
                  class="q-mr-sm"
                  @click="reinstallContents"
                />
              </template>
            </q-table>
```
* The `reinstallContents()` function :

```js
 reinstallContents: function () {
      let contentIds = _.map(this.broken_selected, e => e.content_id)
      this.triggerContentsTargetStateChange({bundle: this.$route.params.bundle, contentIds, newState: 'installed'})
      this.broken_selected = []
    }
```
* The `triggerContentsTargetStateChange()` function :

```js
 export const triggerContentsTargetStateChange = encase(['backendApi'], (backendApi) => async ({commit, state}, {bundle, contentIds, newState}) => {
  let app = _.find(state.installed_applications, a => a.bundle === bundle)

  contentIds.forEach(async cId => {
    let content = _.find(app.contents, c => c.content_id === cId)

    let targetStateUrl = content.targetStateUrl

    let res = await backendApi.setContentTargetState(targetStateUrl, newState)

    if (typeof res.data.message !== 'undefined') {
      Notify.create({
        message: res.data.message
      })
    } else {
      commit(mutationTypes.CONTENT_INSTALLATION_STARTED, {bundle, cId, newState: newState})
    }
  })
})
```

* And to finish the call to the api through the `setContentTargetState()` function:

```js
async setContentTargetState (contentTargetState, targetState, targetVersion) {
    let state = {
      'target_state': targetState
    }
    if (targetVersion) {
      state.target_version = targetVersion
    }
    try {
      await this.axios.put(contentTargetState, state)
      return
    } catch (e) {
      return e.response
    }
  }
```
