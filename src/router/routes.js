export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      { path: '',
        redirect: '/home',
        meta: {
          isPublic: true
        }
      },
      { path: '/home',
        component: () => import('pages/home/home'),
        meta: {
          isPublic: true
        }
      },
      { path: '/aboutus',
        component: () => import('pages/aboutus/aboutus'),
        meta: {
          isPublic: true
        }
      },
      {
        path: '/home/categories/:categoryIndex',
        component: () => import('pages/home/category'),
        meta: {
          isPublic: true
        }
      },
      {
        path: '/home/categories/:categoryIndex/playlist/:playlistIndex',
        component: () => import('pages/home/playlist'),
        meta: {
          isPublic: true
        }
      },
      {
        path: '/home/playlist/:playlistIndex',
        component: () => import('pages/home/playlist'),
        meta: {
          isPublic: true
        }
      },
      {
        path: '/oidc-callback', // Needs to match redirect_uri in you oidcSettings
        name: 'oidcCallback',
        component: () => import('components/oidc-callback'),
        meta: {
          isVuexOidcCallback: true,
          isPublic: true
        }
      },
      {
        path: '/silent-renew',
        component: () => import('components/silent-renew')
      }
    ]
  },
  {
    path: '/applications',
    component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/applications/applications') },
      { path: ':bundle', component: () => import('pages/applications/application_details') },
      { path: ':bundle/contents', component: () => import('pages/applications/contents') },
      { path: ':bundle/configuration', component: () => import('pages/applications/configuration') }
    ]
  },
  {
    path: '/installed',
    component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/applications/installed_applications') }
    ]
  },
  {
    path: '/process',
    component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/process/process') }
    ]
  },
  {
    path: '/users',
    component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/users/list') }
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/categories',
    component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/categories/list') },
      { path: 'add', component: () => import('pages/categories/add') },
      { path: 'edit/:categoryIndex', component: () => import('pages/categories/add') }
      // { path: 'thesaurus', component: () => import('pages/categories/thesaurus') }
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/playlists',
    component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/playlists/list') },
      { path: 'add', component: () => import('pages/playlists/add') },
      { path: 'edit/:playlistIndex', component: () => import('pages/playlists/add') }
    ],
    meta: {
      requiresAuth: true
    }
  },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
