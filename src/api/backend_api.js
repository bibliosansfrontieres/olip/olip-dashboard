'use strict'

import URI from 'urijs'

export default class BackendApi {
  constructor (axios, config) {
    this.axios = axios
    this.config = config
  }

  async listApplications (currentState, visible) {
    let config = await this.config()

    let url = URI(config.api_url + '/applications/')

    if (currentState) {
      url.addQuery('current_state', currentState)
    }

    if (visible) {
      url.addQuery('visible', visible)
    }

    let req = await this.axios.get(url)

    return req.data.data
  }

  async getAppDetail (appUrl) {
    let req = await this.axios.get(appUrl)

    return req.data
  }

  async getAppTargetState (appTargetStateUrl) {
    let req = await this.axios.get(appTargetStateUrl)

    return req.data
  }

  async setAppTargetState (appTargetStateUrl, targetState, targetVersion) {
    let state = {
      'target_state': targetState
    }

    if (targetVersion) {
      state.target_version = targetVersion
    }

    await this.axios.put(appTargetStateUrl, state)
  }

  async setAppDisplaySettings (appDisplaySettingUrl, visibility, weight) {
    await this.axios.put(appDisplaySettingUrl, {
      visible: visibility,
      weight: weight
    })
  }

  async getAppContent (appContentUrl) {
    let content = await this.axios.get(appContentUrl)

    return content.data.data
  }

  async setContentTargetState (contentTargetState, targetState, targetVersion) {
    let state = {
      'target_state': targetState
    }

    if (targetVersion) {
      state.target_version = targetVersion
    }
    try {
      await this.axios.put(contentTargetState, state)
      return
    } catch (e) {
      return e.response
    }
  }

  async listUsers () {
    let config = await this.config()

    let req = await this.axios.get(config.api_url + '/users/')

    return req.data.data
  }

  async saveUser (username, user) {
    let config = await this.config()

    let req = await this.axios.put(config.api_url + '/users/' + username, user)

    return req.data
  }

  async deleteUser (username) {
    let config = await this.config()

    await this.axios.delete(config.api_url + '/users/' + username)
  }

  async listTerms () {
    let config = await this.config()

    let req = await this.axios.get(config.api_url + '/terms/')

    return req.data.data
  }

  async listCategories () {
    let config = await this.config()
    let req = await this.axios.get(config.api_url + '/categories/')
    return req.data.data
  }

  async createCategory (data) {
    let config = await this.config()
    let req = await this.axios.post(config.api_url + '/categories/', data)
    return req.data
  }

  async updateCategory (categoryUrl, data) {
    let req = await this.axios.put(categoryUrl, data)

    return req.data
  }

  async updateThumbnail (categoryThumbnailUrl, contentType, contentLength, content, progress) {
    const config = {
      onUploadProgress: progressEvent => {
        progress(progressEvent.loaded)
      },
      headers: {
        'Content-Type': contentType,
        'Content-Length': contentLength
      }
    }
    await this.axios.put(categoryThumbnailUrl, content, config)
  }

  async getCategory (categoryUrl) {
    let req = await this.axios.get(categoryUrl)

    return req.data
  }

  async createTerm (term) {
    let config = await this.config()

    let req = await this.axios.post(config.api_url + '/terms/', {term: term})

    return req.data
  }

  async deleteTerm (termUrl) {
    await this.axios.delete(termUrl)
  }

  async deleteCategory (categoryId) {
    let config = await this.config()

    await this.axios.delete(config.api_url + '/categories/' + categoryId)
  }

  async listApplicationEndpoints (endpointsUrl, options) {
    let url = new URI(endpointsUrl)

    let withContainers = options.withContainers
    let withContents = options.withContents
    let categoryId = options.categoryId
    let playlistId = options.playlistId

    if (withContainers) {
      url.addQuery('with_containers', withContainers)
    }

    if (withContents) {
      url.addQuery('with_contents', withContents)
    }

    if (categoryId) {
      url.addQuery('category_id', categoryId)
    }

    if (playlistId) {
      url.addQuery('playlist_id', playlistId)
    }

    let req = await this.axios.get(url)

    return req.data.data
  }

  async getPlaylist (playlistUrl) {
    let req = await this.axios.get(playlistUrl)

    return req.data
  }

  async listPlaylists (categoryId, visibleForUser) {
    let config = await this.config()

    let url = new URI(config.api_url + '/playlists/')

    if (categoryId) {
      url.addQuery('category_id', categoryId)
    }

    if (visibleForUser) {
      url.addQuery('visible_for_user', visibleForUser)
    }

    let req = await this.axios.get(url)

    return req.data.data
  }

  async updatePlaylist (playlistUrl, data) {
    let req = await this.axios.put(playlistUrl, data)

    return req.data
  }

  async createPlaylist (data) {
    let config = await this.config()

    let req = await this.axios.post(config.api_url + '/playlists/', data)

    return req.data
  }

  async deletePlaylist (playlistId) {
    let config = await this.config()

    await this.axios.delete(config.api_url + '/playlists/' + playlistId)
  }

  async getConfiguration (configurationUrl) {
    let req = await this.axios.get(configurationUrl)

    return req.data
  }

  async putConfiguration (configurationUrl, configuration) {
    let req = await this.axios.put(configurationUrl, configuration)

    return req.data
  }

  async search (searched) {
    let config = await this.config()

    let req = await this.axios.get(config.api_url + `/opensearch/search?q=${searched}`, {
      responseType: 'document'
    })

    return req.data
  }

  async getSysinfo () {
    let config = await this.config()

    let req = await this.axios.get(config.api_url + '/applications/sysinfo')

    return req.data
  }

  async getPlaylistLinkIconUrl (playlistId, linkIndex) {
    let config = await this.config()
    return `${config.api_url}/playlists/${playlistId}/link-icon/${linkIndex}`
  }

  async deletePlaylistLinkIcon (playlistId, linkIndex) {
    let url = await this.getPlaylistLinkIconUrl(playlistId, linkIndex)
    await this.axios.delete(url)
  }

  async getCategoryLinkIconUrl (categoryId, linkIndex) {
    let config = await this.config()
    return `${config.api_url}/categories/${categoryId}/link-icon/${linkIndex}`
  }

  async deleteCategoryLinkIcon (categoryId, linkIndex) {
    let url = await this.getCategoryLinkIconUrl(categoryId, linkIndex)
    await this.axios.delete(url)
  }

  async setPlaylistDisplaySettings (playlistId, weight) {
    let config = await this.config()
    await this.axios.put(config.api_url + '/playlists/display-settings', {
      id: playlistId,
      weight: weight
    })
  }

  async setCategoryDisplaySettings (categoryId, weight) {
    let config = await this.config()
    await this.axios.put(config.api_url + '/categories/display-settings', {
      id: categoryId,
      weight: weight
    })
  }

  async setAdminLogo (contentType, contentLength, content, progress) {
    let defaultConfig = await this.config()
    let logoThumbnailUrl = defaultConfig.api_url + '/logo/admin/thumbnail'
    const config = {
      onUploadProgress: progressEvent => {
        progress(progressEvent.loaded)
      },
      headers: {
        'Content-Type': contentType,
        'Content-Length': contentLength
      }
    }
    await this.axios.put(logoThumbnailUrl, content, config)
  }

  async getAdminLogo () {
    let config = await this.config()

    let req = await this.axios.get(config.api_url + '/logo/admin/thumbnail')
    return req.config.url
  }
}
