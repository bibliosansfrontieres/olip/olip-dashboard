export default {
  Language: 'ቋንቋ',
  Search: 'ፈልግ',
  Home: 'ዋና ገጽ',
  Catalog: 'የመዝገብ ዝርዝር',
  Applications: 'መተግበሪያዎች',
  Application: 'መተግበሪያ',
  Users: 'ተጠቃሚዎች',
  Categories: 'ፈርጆች',
  Playlists: 'አጫዋች ዝርዝሮች',
  Thesaurus: 'የተመሳሳይ እና ተቃራኒ ቃላት መዝገበ-ቃላት',
  Name: 'ስም',
  Description: 'መግለጫ',
  Size: 'መጠን',
  Subject: 'ጉዳዩ',
  Bundle: 'ጥቅል',
  Order: 'ድርደራ',
  Cancel: 'አቋርጥ',
  Save: 'አስቀምጥ',
  More: 'ተጨማሪ',
  Edit: 'አርትዕ',
  Editing: 'አርትኦት',
  Upload_thumbnail: 'ጥፍር አከል ይጫኑ',
  Tags: 'መለያዎች',
  Title: 'ርዕስ',
  Cant_find: 'ሊያገኙት አልቻሉም?',
  Pinned: 'ተሰክቷል',
  Login: 'ግባ',
  Logoff: 'ጨርሰህ ውጣ',
  Password: 'የይለፍ ቃል',
  Help: 'እገዛ',
  FAQ: 'በተደጋጋሚ የሚነሱ ጥያቄዎች',
  bytes: 'ባይቶች',
  by: 'በ',
  Upload_logo: 'አርማ ይስቀሉ',
  Delete_category: 'ምድብ ሰርዝ',
  Delete_playlist: 'አጫዋች ዝርዝር ይሰርዙ',
  Edite_category: 'ምድቡን ያርትዑ',
  Edite_playlist: 'አጫዋች ዝርዝር አርትዕ',
  state: {
    id: 'ሁኔታ',
    installing: 'በመጫን ላይ ...',
    uninstalled: 'ተጭኖ የነበረው ጠፍቷል',
    Uninstalled: 'ተጭኖ የነበረው ጠፍቷል',
    installed: 'ተጭኗል',
    uninstalling: 'የተጫነውን በማጥፋት ላይ',
    upgrading: 'በማሻሻል ላይ ...',
    downloaded: 'ወርዷል',
    downloading: 'በማውረድ ላይ',
    download: 'አውርድ',
    stopping: 'በማቆም ላይ ...',
    removing: 'በማስወገድ ላይ ...',
    Delete: 'ሰርዝ',
    Deleted: 'ተሰርዟል',
    Install: 'ጫን',
    Uninstall: 'የተጫነውን አጥፋ',
    Upgrade: 'አሻሽል',
    visible: 'የሚታይ',
    broken: 'የተሰበረ'
  },
  content: {
    content: 'ይዘት',
    contents: 'ይዘቶች',
    add: 'ይዘት ያክሉ',
    select: 'ይዘት ይምረጡ',
    no_contents: 'ምንም ይዘት የለም።',
    title: 'ለመተግበሪያ የይዘት ዝርዝር ',
    list: 'የይዘት ዝርዝር',
    available: 'የሚገኙ ይዘቶች ዝርዝር',
    installed: 'የተጫነ ይዘት ዝርዝር',
    upgradable: 'ሊሻሻሉ የሚችሉ ይዘቶች ዝርዝር',
    broken: 'የተሰበረ ይዘት ዝርዝር',
    reinstall: 'እንደገና ጫን'
  },
  application: {
    search: 'መተግበሪያዎችን ይፈልጉ',
    installed: 'የተጫኑ መተግበሪያዎች',
    downloaded: 'የወረዱ መተግበሪያዎች',
    uninstall: 'መተግበሪያ ያጥፉ',
    uninstall_confirm: 'እባክዎ ይህን መተግበሪያ ማጥፋት መፈለግዎን ያረጋግጡ።',
    uninstall_confirmation: 'መተግበሪያዎ በተሳካ ሁኔታ ጠፍቷል።',
    will_install: 'መተግበሪያዎ ይጫናል።',
    Delete: 'መተግበሪያ ሰርዝ',
    deleted_confirm: 'እባክዎ ይህን መተግበሪያ መሰረዝ እንደሚፈልጉ ያረጋግጡ',
    deleted_confirmation: 'መተግበሪያዎ በተሳካ ሁኔታ ተሰርዟል።',
    select_existing: 'እባክዎ አንድ ነባር መተግበሪያ ይምረጡ'
  },
  category: {
    category: 'ፈርጅ',
    search: 'ፈርጅ ይፈልጉ',
    add: 'ፈርጅ ያክሉ',
    Delete: 'ፈርጅ ሰርዝ',
    delete_confirm: 'እባክዎ ይህንን ፈርጅ መሰረዝ መፈለግዎን ያረጋግጡ።',
    delete_confirmation: 'ፈርጅዎ በተሳካ ሁኔታ ተሰርዟል።',
    content_is_present: 'ይዘቱ አስቀድሞ በፈርጅ ውስጥ አለ፡፡',
    select_playlist: 'እባክዎ አንድ ነባር አጫዋች ዝርዝር ይምረጡ'
  },
  playlist: {
    search: 'አጫዋች ዝርዝር ይፈልጉ',
    add: 'አጫዋች ዝርዝር ያክሉ',
    Delete: 'የጨዋታ ዝርዝርን ሰርዝ',
    delete_confirm: 'እባክዎ ይህን አጫዋች ዝርዝር ለመሰረዝ መፈለግዎን ያረጋግጡ።',
    delete_confirmation: 'አጫዋች ዝርዝርዎ በተሳካ ሁኔታ ተሰርዟል።',
    about_pin: 'የተሰኩ አጫዋች ዝርዝሮች በመነሻ ማያ ገጽ ላይ ይታያሉ ፡፡',
    content_exist: 'ይዘቱ አስቀድሞ በፈርጅ ውስጥ አለ፡፡'
  },
  tag: {
    add: 'መለያ ያክሉ',
    Delete: 'መለያ ይሰርዙ',
    delete_confirm: 'እባክዎ ይህንን መለያ መሰረዝ መፈለግዎን ያረጋግጡ።',
    delete_confirmation: 'አጫዋች ዝርዝርዎ በተሳካ ሁኔታ ተሰርዟል።'
  },
  about: {
    us: 'ስለ እኛ',
    disclaimer: 'ኦ.ኦ.አይ.ፒ. ክፍት የሆነ ሶፍትዌር ነው በ',
    our_identity: 'ማንነታችን',
    our_statement: 'መግለጫችን',
    what: 'ኦ.ኤል.አይ.ፒ  ለሁሉም ክፍት የሆነ እና ማንኛውንም መሳርያ ያለ ኢንተርኔት ወደሚሰራ ሰርቨር መቀየር የሚችል በማኅበሩ የተሰራ ሶፍትዌር ነው፡፡',
    tempe_declaration: 'የ ‹ጊዜ› አዋጁ መግለጫ የሕብረት ሥራ ማህበሩ መነሻ መግለጫ ነው::',
    read_declaration: 'ሙሉውን መግለጫውን ያንብቡ',
    our_committee: 'ኮሚቴያችን',
    our_supporters: 'ደጋፊዎቻችን',
    shareit: 'የኦ.ኤል.አይ.ፒ ልማት እውን የሆነው ከ Share-IT አቀላጣፊ በሚገኘው ድጋፍ ነው፡፡',
    with_support: 'በሚገኝ ድጋፍ'
  },
  user: {
    list: 'የተጠቃሚዎች ዝርዝር',
    search: 'ተጠቃሚ ይፈልጉ',
    add: 'ተጠቃሚ ይጨምሩ',
    adding: 'ተጠቃሚን በመጨመር ላይ',
    name: 'የተጠቃሚ ስም',
    helper: 'ፊደሎች ፣ ቁጥሮች ፣ ነጥብ እና ዳሽ ቁምፊ ብቻ',
    helper_error: 'እባክዎ ትክክለኛ የተጠቃሚ ስም ያስገቡ',
    Admin: 'አስተዳዳሪ'
  }
}
