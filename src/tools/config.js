import axios from 'axios'

var defaultConfig = {
  // api_url: 'http://192.168.6.246:5002',
  // api_url: 'http://70.167.220.187:443',
  api_url: 'http://localhost:5002',
  supported_languages: ['en', 'fr', 'ar', 'bn', 'my', 'rhg', 'es', 'sw', 'am', 'el', 'fa', 'ps', 'hi', 'run', 'ru', 'uk', 'ro', 'ru']
}

var cnf = defaultConfig

var configSet = false

function getConfig () {
  return new Promise((resolve, reject) => {
    if (configSet) {
      resolve(cnf)
    } else {
      configSet = true

      axios
        .get('/config.json')
        .then((res) => {
          let cfg = res.data

          for (var k in cfg) cnf[k] = cfg[k]
          resolve(cnf)
        })
        .catch(() => {
          console.log('Failed to get config from url. Providing default config')
          resolve(cnf)
        })
    }
  })
}

export default getConfig
