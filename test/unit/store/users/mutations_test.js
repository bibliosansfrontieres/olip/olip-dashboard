import chai from 'chai'
import * as mutationTypes from '../../../../src/store/users/mutation_types'
import mutations from '../../../../src/store/users/mutations';

var expect = chai.expect

describe('Users', function() {
  describe('Mutations', function() {
    describe('#listed_users()', function() {
      it('should set the users property of the state', function() {
        // given
        let users = [{
          username: "john.doe",
          name: "John Doe",
          admin: true,
          links: [
            { rel: 'self', href: "http://localhost:8084/users/john.doe"}
          ]
        },
        {
          username: "jane.doe",
          name: "Jane Doe",
          provider: "test.provider",
          links: [
            { rel: 'self', href: "http://localhost:8084/users/jane.doe"}
          ]
        }]

        let payload = {
          type: mutationTypes.LISTED_USERS,
          users: users
        }
        let state = {}
        // when
        mutations.listed_users(state, payload)

        // then
        expect(state.users).to.eql([{
          username: "john.doe",
          name: "John Doe",
          admin: true
        },
        {
          username: "jane.doe",
          name: "Jane Doe",
          provider: "test.provider"
        }])
      })
    })

    describe('#user_added()', function() {
      it('should add a user to the users state', function() {
        // given
        let user = {
          username: "john.doe",
          name: "John Doe",
          admin: true,
          links: [
            { rel: 'self', href: "http://localhost:8084/users/john.doe"}
          ]
        }

        let payload = {
          type: mutationTypes.USER_ADDED,
          user: user
        }
        let state = { users: []}
        // when
        mutations.user_added(state, payload)

        // then
        expect(state.users).to.eql([{
          username: "john.doe",
          name: "John Doe",
          admin: true
        }])
      })

      it('should update an existing user if already existing', function() {
         // given
         let user = {
          username: "john.doe",
          name: "John Doe",
          admin: true,
          links: [
            { rel: 'self', href: "http://localhost:8084/users/john.doe"}
          ]
        }

        let payload = {
          type: mutationTypes.USER_ADDED,
          user: user
        }
        let state = { users: [
          {
            username: "john.doe",
            name: "Old john name",
            admin: true
          }
        ]}
        // when
        mutations.user_added(state, payload)

        // then
        expect(state.users).to.eql([{
          username: "john.doe",
          name: "John Doe",
          admin: true
        }])
      })
    })
  })
  
})
