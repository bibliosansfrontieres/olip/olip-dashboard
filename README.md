# OLIP Dashboard

[![pipeline status](https://gitlab.com/bibliosansfrontieres/olip/olip-dashboard/badges/master/pipeline.svg)](https://gitlab.com/bibliosansfrontieres/olip/olip-dashboard/commits/master)

## What is olip
OLIP is an open source software developed by the Offline Internet Consortium to turn any device into an offline server. It makes it easy to install WebApps and manage their content on your server.
here is the olip dashboard documentation, writen in order to  make it easier to get to grips with the code source.

For further information about what is OLIP, [check out documentation](http://bibliosansfrontieres.gitlab.io/olip/olip-documentation/olip/).

## Discover olip dahsboard
Olip is divided into 2 projects: dashboard and an api.
In this documentation we will only talk about the dashboard and its specificities.

Olip dashboard was designed with quasar JS  [v0.17.26](https://v0-17.quasar-framework.org/), which is an open-source framework based on Vue.js under MIT license, it allows you, as a web developer, to quickly create responsive websites/applications. here is his latest published version [v2.5.5](https://quasar.dev/introduction-to-quasar) .

Discover [here](https://gitlab.com/bibliosansfrontieres/olip/olip-user-documentation/-/wikis/en/What-is-OLIP%3F) olip dashboard and how to use it.

#### Site map
![site map](src/statics/siteMap.png)

## Get the source code
Run this command to download olip dashboard on your terminal:

```shell
git clone https://gitlab.com/bibliosansfrontieres/olip/olip-dashboard.git

cd olip-dahboard
```
After starting Olip api, run the following command to launch your olip dashboard.

```shell
npm install
quasar dev
```
your developpement server will start on  [http://localhost:8082/
](http://localhost:8082/)

## Features
On olip dashboard you can do some of those actions :

###### *Click on each of these features in the list below to find out how they work*

- [Download / install applications](src/pages/applications/CONTRIBUTING.md#download-an-application)
- [Install application contents](src/pages/applications/CONTRIBUTING.md#user-content-install-an-application-content)
- [Create and manage a category / playlist](src/pages/categories/CONTRIBUTING.md#user-content-create-a-category)
- [Create and manage a user account](src/pages/users/CONTRIBUTING.md)


