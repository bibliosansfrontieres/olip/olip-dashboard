# hadolint ignore=DL3007
FROM offlineinternet/olip-base:latest as builder

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]
RUN ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')" && \
	case $ARCH in \
		armhf) node_build='https://nodejs.org/dist/v16.8.0/node-v16.8.0-linux-armv7l.tar.gz' ;; \
		amd64) node_build='https://nodejs.org/dist/v16.8.0/node-v16.8.0-linux-x64.tar.gz' ;; \
		i386) node_build='https://unofficial-builds.nodejs.org/download/release/v16.4.2/node-v16.4.2-linux-x86.tar.gz' ;; \
		arm64) node_build='https://nodejs.org/dist/v16.8.0/node-v16.8.0-linux-arm64.tar.gz' ;; \
		*) echo >&2 "error: unsupported architecture: $ARCH"; exit 1 ;; \
	esac && \
	wget --quiet $node_build -O node.tar.gz && \
	tar -C /usr/local -xzf node.tar.gz --strip=1

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]
# hadolint ignore=DL3008
RUN set -eux; \
	ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
	if [ "$ARCH" = "armhf" ]; then \
		apt-get update; \
		apt-get install --yes --quiet --quiet --no-install-recommends libatomic1 ; \
	fi

WORKDIR /opt/build

COPY . /opt/build

RUN npm install && \
    npm install -g quasar quasar-cli && \
    quasar build


# hadolint ignore=DL3007
FROM offlineinternet/olip-base:latest

# hadolint ignore=DL3008
RUN apt-get update \
	&& apt-get install --yes --quiet --quiet  --no-install-recommends nginx \
		&& apt-get clean \
		&& rm -rf /var/lib/apt/lists/*

COPY --from=builder /opt/build/dist/* /usr/share/nginx/html/

COPY docker/nginx.conf /etc/nginx/nginx.conf

ENV API_URL=http://localhost:5002

COPY docker/startup.sh /startup.sh

ENTRYPOINT ["bash", "/startup.sh"]

CMD ["nginx", "-g", "daemon off;"]
