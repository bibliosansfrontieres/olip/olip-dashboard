# Deploy a new instance 

```
cd charts
kubectl create namespace my_project_name
helm upgrade  --install --namespace="my_project_name" --set domainName=example.com my_project_name .
```

## Set storage space 

read `values.yaml` to override values from command line.

You can define persistente storage for your instance, here with 100Go of disk space

```
helm upgrade --install --namespace="my_project_name" --set persistentStorage.enabled="true" --set persistentStorage.olipStorageSize="100Gi" --set domainName=example.com my_project_name .
```

# Reach the instance

* OLIP Dashboard : http://my_project_name.example.com
* OLIP API : http://api.my_project_name.example.com
* Mediacenter : http://mediacenter.my_project_name.example.com
* Statistics : http://stats.my_project_name.example.com (only available when `persistentStorage.enabled="true"`)
* etc.
# Destroy the instane
```
helm delete my_project_name
kubectl delete namespaces my_project_name
```

# Technical details 

We use a Traefik2 Scaleway loadbalancer deployed with https://www.scaleway.com/en/docs/tutorials/traefik-v2-cert-manager/?facetFilters=%5B%22categories%3Akubernetes%22%5D&page=1#creating-a-service-to-deploy-a-loadbalancer-in-front-of-traefik-2

`traefik-loadbalancer.yml`
```
apiVersion: v1
kind: Service
metadata:
  name: traefik-ingress
  namespace: kube-system
  labels:
    k8s.scw.cloud/ingress: traefik2
spec:
  type: LoadBalancer
  ports:
    - port: 80
      name: http
      targetPort: 8000
    - port: 443
      name: https
      targetPort: 8443
  selector:
    app.kubernetes.io/name: traefik
```

`kubectl create -f traefik-loadbalancer.yml`

---
Enable access log for Traefik Ingress. Apply the file named `traefik-patch.yml` found in this directory

`kubectl patch daemonset traefik -n kube-system --type merge --patch "$(cat traefik-patch.yml)"`

---

Each domain name used in deployment must point to the load balancer public IP address found in Scaleway console management https://console.scaleway.com/load-balancer/ips